#!/usr/bin/env python3
"""
Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""
from jinja2 import Template


def convert_file(path: str, params: dict):
    """Render the given path with the given parameters in-place"""
    with open(path, "r+") as f:
        data = Template(f.read()).render(params)
        f.seek(0)
        f.truncate()
        f.write(data)


if __name__ == "__main__":
    # Add files to convert here
    convert_file("roles/containers_nginx/templates/apparmor.j2",
                 {"nginx_folder_mapping": [{"alias": "/srv/test"}]})
    convert_file("roles/containers_wagtail/templates/apparmor.j2",
                 {"wagtail_domain": "example.com"})
