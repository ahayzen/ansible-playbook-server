This repository provides a server collection of roles to be used in a playbook.
It is intended to be used in combination with the `headless` collection.

# Usage

Add collection to `requirements.yml` file

```
collections:
  - name: https://gitlab.com/ahayzen/ansible-playbook-server
```

Then pull the collection for use

```
ansible-galaxy collection install -r requirements.yml --force
```

Then add the roles to your playbook

```
- name: playbook
  ...
  roles:
    - role: ahayzen.server.role
```

# Backup

The backup playbook will create a backup in the folder `/tmp/ansible-backup/<timestamp>/`.

# Restore

The restore playbook will use a backup from the folder `/tmp/ansible-backup/restore/`.

The following files are expected to exist in the restore folder:

```
/tmp/ansible-backup/restore/postgres.wagtail.bak
/tmp/ansible-backup/restore/wagtail.apikeys.py
/tmp/ansible-backup/restore/wagtail.media.tar.bz2
```

# Timers

The following timers are defined within the playbook. Note that all timers have `RandomizedDelaySec=60m` set.

Time | Name | Purpose
-----|------|--------
Friday 22:00 | `containers.letsencrypt.renewal.timer` | Renew letsencrypt certificate

# Container Users

The following container users are defined within the playbook. Note that these users are mapped into the subuid range of `unprivusr` (which is mapped to 900000).

## Users

Users uid's take the form `(9)1XXYY` where `XXYY` is the number of the first two letters of the container name.

Container | User | uid host | uid container | Groups
----------|------|----------|---------------|-------
letsencrypt | `container-letsencrypt` | `911407` | `11407` | `wwwdata`
nginx | `container-nginx` | `911407` | `11407` | `wwwdata`
postgres | `container-postgres` | `911615` | `11615` | `postgresclient`
wagtail | `container-wagtail` | `912301` | `12301` | `postgresclient`, `wwwdata`

Note that letsencrypt and nginx share the same uid, so that generated certificates can be read while still using 0600.

## Groups

Groups gid's take the form `(9)5XXYY`, where `XXYY` is the number of the first two letters of the group.

Group | gid host | gid container | Purpose
------|----------|---------------|--------
container-postgresclient | `951615` | `51615` | ssl certs have read perms so that clients can use it
container-wwwdata | `952323` | `52323` | data files have read perms so nginx can use it
