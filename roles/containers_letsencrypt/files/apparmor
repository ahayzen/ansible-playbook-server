#include <tunables/global>


profile containers-letsencrypt flags=(attach_disconnected,mediate_deleted) {
  #include <abstractions/base>

  #
  # Networking
  #

  # We need networking
  network inet tcp,

  deny network inet udp,
  deny network inet icmp,
  deny network raw,
  deny network packet,

  #
  # Capabilities
  #

  capability chown,

  #
  # Mounts
  #

  umount,
  deny mount,

  #
  # Filepaths
  #

  # FIXME: Some docker overlay paths aren't resolved by apparmor
  /var/lib/docker/*/overlay2/*/diff/usr/lib/python3{,.?}/{,**} rm,
  /var/lib/docker/*/overlay2/*/diff/usr/{,local/}bin/ r,
  /var/lib/docker/*/overlay2/*/diff/usr/local/lib/python3{,.?}/dist-packages/{,**} rm,

  # Allow cp,echo,mkdir,rm,sh as our entrypoint uses this
  /bin/{cp,echo,mkdir,rm,sh} rix,

  # We use stderr / stdout
  /dev/tty rw,

  # Allow changing user
  /etc/{group,nsswitch.conf,passwd} r,

  # Allow DNS
  /etc/{gai.conf,hosts,host.conf,resolv.conf} r,

  # Allow creating live certificates
  /etc/letsencrypt/.certbot.lock rwk,
  /etc/letsencrypt/** rw,

  # Python needs mime types and inputrc
  /etc/{inputrc,mime.types} r,

  # Allow reading of ssl config
  /etc/ssl/** r,

  # Allow writing of file to signify that a renewal has occurred
  /run/renewal.check rw,

  # Allow using ldconfig
  /sbin/ldconfig ix,

  # Allow writing to acme challenge folder
  /srv/letsencrypt/** rw,

  # Python needs to write core and temporary files
  /tmp/{core,**} rw,

  # Allow using certbot and reading of bin dir
  /usr/{,local/}bin/{,certbot} rix,

  # Allow reading of python libs
  /usr/lib/python3{,.?}/{,**} rm,
  /usr/local/lib/python3{,.?}/dist-packages/{,**} rm,

  # Allow executing docker entrypoint binary
  /usr/local/bin/docker-entrypoint.sh rix,

  # Allow reading of python3 packages
  /usr/local/lib/python3{,.?}/dist-packages/{,**} rm,

  # Allow writing to work and log directory
  /var/{lib,log}/letsencrypt/.certbot.lock rwk,
  /var/{lib,log}/letsencrypt/{,*} rw,

  # Python needs to access fds/mounts etc of processes
  owner @{PROC}/[0-9]*/fd/{,**} r,
  owner @{PROC}/[0-9]*/mounts r,
}
