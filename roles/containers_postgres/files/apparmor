#include <tunables/global>


profile containers-postgres flags=(attach_disconnected,mediate_deleted) {
  #include <abstractions/base>

  #
  # Networking
  #

  # We need networking
  network inet tcp,
  network inet udp,

  deny network inet icmp,
  deny network raw,
  deny network packet,

  #
  # Capabilities
  #

  capability chown,
  capability setuid,
  capability setgid,

  # So postgres can bind to a port
  capability net_bind_service,

  #
  # Mounts
  #

  umount,
  deny mount,

  #
  # Filepaths
  #

  # FIXME: Some docker overlay paths aren't resolved by apparmor
  /var/lib/docker/*/overlay2/*/diff/etc/postgresql/11/{,**} r,
  /var/lib/docker/*/overlay2/*/diff/lib/{,**} r,
  /var/lib/docker/*/overlay2/*/diff/usr/lib/locale/{,**} r,
  /var/lib/docker/*/overlay2/*/diff/usr/lib/postgresql/{,**} r,
  /var/lib/docker/*/overlay2/*/diff/usr/share/zoneinfo/{,**} r,

  # Postgres tries to file_mmap in the root directory ?
  # for now deny this to silence logs
  deny / rw,

  # Allow executing of common tools which we use in our entrypoint and other commands
  /bin/{bash,chmod,chown,cp,dash,date,grep,ldd,mkdir,mktemp,rm,sed,touch,which} rix,

  # Allow writing to shared memory
  /dev/shm/PostgreSQL* rw,

  # We use stderr / stdout
  /dev/tty rw,

  # Allow changing user
  /etc/{group,nsswitch.conf,passwd} r,

  # Allow DNS
  /etc/{gai.conf,hosts,host.conf,resolv.conf} r,

  # Allow reading of ext ssl file to create root cert
  /etc/ssl/openssl.cnf r,

  # Allow reading postgres config
  /etc/postgresql/** r,
  /etc/postgresql-common/** r,

  # Allow reading of lib
  /lib/{,**} r,
  /lib/x86_64-linux-gnu/ld-2.28.so rix,

  # Allow writing for copying public certificates to docker volume
  /opt/containers.postgres/ssl/{,*} rw,

  # Allow writing so that postgres can create a lock file
  /run/postgresql/{,**} rw,

  # Allow executing of common tools which we use in our entrypoint and other commands
  /usr/bin/{ldd,locale,mawk,printf} rix,

  # Allow executing of openssl (for creating certs) and ssl-cert-check (to verify them)
  /usr/bin/{openssl,ssl-cert-check} rix,

  # Allow reading of locale data
  /usr/lib/locale/{,**} r,

  # Allow executing of postgres tools
  /usr/lib/postgresql/11/bin/{createdb,initdb,pg_ctl,postgres,psql} rix,

  # Allow reading and exectuing docker entrypoint
  /usr/local/bin/docker-entrypoint.sh rix,

  # Allowing reading of perl files
  /usr/share/perl5/** r,

  # Allow reading of postgres share files
  /usr/share/postgresql/** r,
  /usr/share/postgresql-common/** rix,

  # Allow reading and writing of postgres volatile folder
  /var/lib/postgresql/** rw,

  # Allow locking of postgres files in pg_wal folder
  /var/lib/postgresql/11/main/pg_wal/** l,

  # Allow writing files in temporary folder
  /var/tmp/** rw,
}
