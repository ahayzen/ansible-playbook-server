---

  # Setup files and folders

  - name: ensure wagtail image folder exists
    file: path="{{ wagtail_dir }}" state=directory group=root mode=0644 owner=root

  - name: copy wagtail docker container files
    template:
      src: "{{ item.src }}"
      dest: "{{ item.dest }}"
      group: "root"
      mode: "{{ item.mode }}"
      owner: "root"
    with_items:
      - { src: "Dockerfile.j2", dest: "{{ wagtail_dir }}/Dockerfile", mode: "0644" }
      - { src: "docker-entrypoint.sh.j2", dest: "{{ wagtail_dir }}/docker-entrypoint.sh", mode: "0755" }
      - { src: "local.py.j2", dest: "{{ wagtail_dir }}/local.py", mode: "0644" }

  - name: install wagtail service and config definition
    template:
      src: "{{ item.src }}"
      dest: "{{ item.dest }}"
      mode: "{{ item.mode }}"
    with_items:
      - { src: "containers.wagtail.service.j2", dest: "/etc/systemd/system/containers.wagtail.service", mode: "0644" }
      - { src: "containers.wagtail.conf.j2", dest: "/opt/containers/conf.d/containers.wagtail.conf", mode: "0644" }
      - { src: "gunicorn_start.j2", dest: "{{ wagtail_dir }}/gunicorn_start", mode: "0755" }

  - name: install wagtail published scheduled timer and config definition
    template:
      src: "{{ item.src }}"
      dest: "{{ item.dest }}"
      mode: "{{ item.mode }}"
    with_items:
      - { src: "containers.wagtail.publish_scheduled.service.j2", dest: "/etc/systemd/system/containers.wagtail.publish_scheduled.service", mode: "0644" }
      - { src: "containers.wagtail.publish_scheduled.timer.j2", dest: "/etc/systemd/system/containers.wagtail.publish_scheduled.timer", mode: "0644" }
    when: wagtail_publish_scheduled

  - name: ensure wagtail media and static directory exists
    file: path="{{ item }}" state=directory group=952323 mode=0751 owner=912301
    with_items:
      - "{{ wagtail_media_dir }}"
      - "{{ wagtail_static_dir }}"

  - name: ensure extra wagtail data directory exists
    file: path="{{ item.host_dir }}" state=directory group=952323 mode=0751 owner=912301
    with_items:
      - "{{ wagtail_extra_data }}"

  - name: ensure that wagtail media directory is not empty
    file:
      # FIXME: Ansible 2.7 has the following properties that resolve the changed_when hack
      # access_time: preserve
      # modification_time: preserve
      group: 952323
      mode: 0640
      owner: 912301
      path: "{{ item }}/data"
      state: touch
    changed_when: False  # for now we need this otherwise every time this is a change
    with_items:
      - "{{ wagtail_media_dir }}"

  - name: ensure wagtail apikeys file exists
    file:
      # FIXME: Ansible 2.7 has the following properties that resolve the changed_when hack
      # access_time: preserve
      # modification_time: preserve
      group: 912301
      mode: 0644
      owner: 912301
      path: "{{ wagtail_api_keys }}"
      state: touch
    changed_when: False  # for now we need this otherwise every time this is a change

  # Load apparmor profile

  - name: copy wagtail apparmor profile
    template:
      src: "apparmor.j2"
      dest: "/etc/apparmor.d/containers-wagtail"
      group: "root"
      mode: "0644"
      owner: "root"
    register: wagtailapparmor

  - name: parse wagtail apparmor profile
    command: "/usr/sbin/apparmor_parser -r -W /etc/apparmor.d/containers-wagtail"
    when: wagtailapparmor.changed

  # Start and enable service

  - name: start and enable wagtail services
    systemd:
      state: started
      name: containers.wagtail.service
      daemon_reload: yes
      no_block: yes
      enabled: yes

  - name: start and enable wagtail publish scheduled timer
    systemd:
      state: started
      name: containers.wagtail.publish_scheduled.timer
      daemon_reload: yes
      no_block: yes
      enabled: yes
    when: wagtail_publish_scheduled
